<?php
script('gpxmotion', 'gpxMotionView');

style('gpxmotion', 'fontawesome-free/css/all.min');
style('gpxmotion', 'view');
style('gpxmotion', 'style');

?>

<div id="app">
    <div id="app-content">
        <?php print_unescaped($this->inc('viewcontent')); ?>
    </div>
</div>
