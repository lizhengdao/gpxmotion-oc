# Aplikácia GpxMotion pre Nextcloud

GpxMotion je aplikácia pre Nextcloud na vytváranie a zobrazovanie animácií ciest na interaktívnej mape.

Pre prezeranie animácie kliknite na tlačidlo "Otvoriť a zobraziť súbor" na hlavnej stránke GpxMotion. Ak nie je nastavená žiadna informácia o animácii, bude použitá predvolená (jedna sekcia 10 sekúnd na trasu/cestu). Ak chýba časová informácia v sekcii animácie a je nastavené "použitie proporcií reálneho času" (ako predvolené to nastavené je), trvanie animácie sa nezmení, ale jej rýchlosť bude proporčná k reálnej rýchlosti.

Pre definovanie animácie navštívte hlavnú stránku GpxMotion a otvorte GPX súbor obsahujúci zoradené trasy/cesty. Potom definujte kroky animácie. Následne skotrolujte, či ste s výsledkom spokojní v náhľade animácie. Potom uložte výsledok do GPX súboru (údaje o animácii sú uložené ako JSON v popisnom poli GPX súboru).

Ak je súbor verejne zdieľaný bez hesla v aplikácii "Súbory", môžete vytvoriť verejný odkaz GpxMotion na animáciu pomocou tlačidla "Zdieľať" na stránke "zobrazenia".

Táto aplikácia je testovaná pre Nextcloud 16 a Firefox a Chromium.

Ocením akúkoľvek spätnú väzbu.

Ak chcete pomôcť s prekladom aplikácie do vášho jazyka navštívte [GpxMotion Crowdin project](https://crowdin.com/project/gpxmotion).

## Inštalácia

Pozrite si [AdminDoc](https://gitlab.com/eneiluj/gpxmotion-oc/wikis/admindoc) pre podrobnosti o inštalácii, integrácii do aplikácie "Súbory".

## Alternatívy

Ak hľadáte alternatívy, pozrite sa aj na:
- [gpxanim](https://github.com/rvl/gpxanim) vytvára video súbor
- [GPX Animator](http://zdila.github.io/gpx-animator/) vytvára video súbor


