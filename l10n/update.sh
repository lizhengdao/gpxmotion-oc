#!/bin/bash

rm -rf /tmp/gpxmotion
git clone https://gitlab.com/eneiluj/gpxmotion-oc /tmp/gpxmotion -b l10n_master
cp -r /tmp/gpxmotion/l10n/descriptions/[a-z][a-z]_[A-Z][A-Z] ./descriptions/
cp -r /tmp/gpxmotion/translationfiles/[a-z][a-z]_[A-Z][A-Z] ../translationfiles/
rm -rf /tmp/gpxmotion

echo "files copied"
